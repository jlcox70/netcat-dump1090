kubernetes
```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: netcat
  name: adsb-netcat-remote
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: netcat-remote
  strategy:
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: netcat-remote
    spec:
      containers:
      - name: netcat-remote
        image: registry.gitlab.com/jlcox70/netcat-dump1090:latest
        imagePullPolicy: Always
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        env:
        - name: SOURCE_HOST
          value: 192.168.1.246
        - name: SOURCE_PORT
          value: "30002"
        - name: DEST_HOST
          value: adsb-feed.dump1090
        - name: DEST_PORT
          value: "30001"
```
