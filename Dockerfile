FROM golang:1.20.0 as exporter
ENV GO111MODULE=on
WORKDIR /app
COPY . .

RUN go get ./
RUN CGO_ENABLED=0 GOOS=linux go build -o bin/dump1090-netcat ./
#RUN go build

FROM scratch
COPY --from=exporter /app/bin/dump1090-netcat /usr/local/bin/
CMD ["/usr/local/bin/dump1090-netcat"]
