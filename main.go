package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
)

func main() {
	// Get source and destination hosts and ports from environment variables
	sourceHost := os.Getenv("SOURCE_HOST")
	sourcePortStr := os.Getenv("SOURCE_PORT")

	destHost := os.Getenv("DEST_HOST")
	destPortStr := os.Getenv("DEST_PORT")

	// Convert port strings to integers
	sourcePort, err := strconv.Atoi(sourcePortStr)
	if err != nil {
		log.Fatal("Invalid SOURCE_PORT:", sourcePortStr)
	}

	destPort, err := strconv.Atoi(destPortStr)
	if err != nil {
		log.Fatal("Invalid DEST_PORT:", destPortStr)
	}

	// Connect to the source host and port
	sourceAddr := fmt.Sprintf("%s:%d", sourceHost, sourcePort)
	sourceConn, err := net.Dial("tcp", sourceAddr)
	if err != nil {
		log.Fatal("Error connecting to source host:", err)
	}
	defer sourceConn.Close()

	// Connect to the destination host and port
	destAddr := fmt.Sprintf("%s:%d", destHost, destPort)
	destConn, err := net.Dial("tcp", destAddr)
	if err != nil {
		log.Fatal("Error connecting to destination host:", err)
	}
	defer destConn.Close()

	// Forward data between the connections
	go forwardData(sourceConn, destConn)
	forwardData(destConn, sourceConn)
}

func forwardData(src, dest net.Conn) {
	buf := make([]byte, 4096)
	for {
		n, err := src.Read(buf)
		if err != nil {
			if err != io.EOF {
				log.Println("Error reading data:", err)
			}
			return
		}

		_, err = dest.Write(buf[:n])
		if err != nil {
			log.Println("Error writing data:", err)
			return
		}
	}
}
